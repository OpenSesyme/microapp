/*==================================================================
							Global Variables
====================================================================*/
var currentNickname = "Owner";
var currentAvatarPath = "Unavailable";
var cartItems = [];

/*==================================================================
	`						Firebase Config`
===================================================================*/
var firebaseConfig = {
	apiKey: "AIzaSyD7xfB4SKgdul3GnSUlyHedoomvJ8FLcKk",
	authDomain: "linda-18e74.firebaseapp.com",
	databaseURL: "https://linda-18e74.firebaseio.com",
	projectId: "linda-18e74",
	storageBucket: "linda-18e74.appspot.com",
	messagingSenderId: "755773494408",
	appId: "1:755773494408:web:8c2f7edf38375f0aa4fb3d",
	measurementId: "G-6VNG2JBB9E"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const db = firebase.firestore();
const BusinessesColl = db.collection('Businesses');

BusinessesColl.get().then((querySnapshot) =>{
	$('.exising-biz').empty();
	if (querySnapshot.size == 0) {
		//Show a message when no existing businesses.
	}
	querySnapshot.forEach((snapshot) =>{
		var data = snapshot.data();
		var bizHtml = `<div class="business shadow-sm">
							<p hidden>${snapshot.id}</p>
							<h4 class="biz-name m-0">${data.name}</h4>
							<small class="biz-location">${data.addressLine1}, ${data.addressCity}</small>
							<div class="biz-description">
								<p>${data.description}</p>
							</div>
							<button type="button" id="view_full_biz" class="order-now-btn">Order Now</button>
						</div>`;
		$('.exising-biz').append(bizHtml);
	});
}).catch((err) =>{
	showSnackbar(err.message);
});

$('.exising-biz').on('click', '#view_full_biz', function(){
	$('#biz_items').show();
	$('#cart_items').hide();
	var modal = document.getElementById("full_biz");
	modal.style.display = "block";

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}

	$('#closeFullBiz').on('click', function(){
		modal.style.display = "none";
	});

	const bizId = $(this).closest('.business').find('p')[0].innerHTML;
	BusinessesColl.doc(bizId).get().then((doc) =>{
		var data = doc.data();
		$('#biz_details_name h2').text(data.name);
		$('#biz_details_name small').text(data.addressLine1 + ", " + data.addressCity);
		var catalogue = data.catalogue;
		if (catalogue == null) {
			catalogue = [];
		}
		$('#show_biz_items').empty();
		catalogue.forEach((item)=>{
			const divId = (item.itemName).replace(' ', '');
			var itemHtml = `<li class="item shadow-sm">
	                			<button type="button" data-toggle="collapse" data-target="#${divId}">
	                				<span id="item_name_show">${item.itemName}</span>
	                				<span class="w3-right" id="item_price_show">R${item.itemPrice}</span>
	                			</button>
	                			<p hidden id="biz_id">${bizId}</p>
	                			<p hidden id="item_obj">${JSON.stringify(item)}</p>
	                			<div id="${divId}" class="collapse">
	                				<div class="item-desdcrption">${item.itemDescription}</div>
									<div class="secify-item d-flex">
										<div class="quantity">
											<div class="value-button" id="decrease" value="Decrease Value">-</div>
											<input type="number" id="number" value="1" />
											<div class="value-button" id="increase" value="Increase Value">+</div>
										</div>
										<div class="w3-right">
											<span class="sub-price">R${item.itemPrice}</span>
										</div>
									</div>
									<div class="adding-to-cart text-center">
										<button type="button" class="add-to-cart-btn">Add to Cart</button>
									</div>
								</div>
	                		</li>`;
	        $('#show_biz_items').append(itemHtml);
		});
	});
});

$('#show_biz_items').on('click', '.add-to-cart-btn', function(){
	var itemObj = JSON.parse($(this).closest('.item').find('#item_obj').text());
	var qty = $(this).closest('.collapse').find('input').val();
	var bizId = $(this).closest('.item').find('#biz_id').text();
	itemObj['qty'] = qty;
	addToCart(itemObj);
});

$('#show_biz_items').on('click', '#increase', function(){
	var value = parseInt($(this).closest('.quantity').find('#number').val(), 10);
	var price = $(this).closest('.item').find('#item_price_show').text();
	price = price.replace('R', '');
	value = isNaN(value) ? 1 : value;
	value++;
	$(this).closest('.quantity').find('#number').val(value);
	var subTotal = (+value * +price).toFixed(2);
	$(this).closest('.secify-item').find('.sub-price').text("R" + subTotal);
})

$('#view_bregistration').on('click', function(){
	var modal = document.getElementById("register_biz");
	modal.style.display = "block";

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}

	$('#closeRegisterBiz').on('click', function(){
		modal.style.display = "none";
	});
});

$('#view_manage_biz').on('click', function(){
	$('#add_item_form').hide();
	var modal = document.getElementById("manage_biz");
	modal.style.display = "block";

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}

	$('#closeManageBiz').on('click', function(){
		modal.style.display = "none";
	});

	var catalogue = [];
	var bizId;
	BusinessesColl.where("owner", "==", "Zulqarnain").onSnapshot((querySnapshot) =>{
		querySnapshot.forEach((snapshot) =>{
			bizId = snapshot.id;
			var data = snapshot.data();
			$('#manage_biz_name_loc h2').text(data.name);
			$('#manage_biz_name_loc small').text(data.addressLine1 + ", " + data.addressCity);
			catalogue = data.catalogue;
			$('#manage_biz_items').empty();
			if (catalogue == null) {
				catalogue = [];
			}
			catalogue.forEach((item)=>{
				var itemHtml = `<li class="added-item">
		                			<div class="w3-row">
		                				<div class="w3-col" style="width: 60%">${item.itemName}</div>
		                				<div class="w3-col" style="width: 25%">R${item.itemPrice}</div>
		                				<div class="w3-col text-right" style="width: 15%">
		                					<i class="ti-trash"></i>
		                				</div>
		                			</div>
		                			<div class="description">${item.itemDescription}</div>
		                		</li>`;
		        $('#manage_biz_items').append(itemHtml);
			});
		});
	});

	$('#manage_biz_items').off('click').on('click', '.ti-trash', function(){
		$(this).closest('li').remove();
	});

	$('#manage_add_items_form').off('submit').on('submit', function(e){
		e.preventDefault();
		var error = false;
		var itemObj = {};
		var formEntries = JSON.parse(JSON.stringify(jQuery('#manage_add_items_form').serializeArray()));
		for (var i = 0; i < formEntries.length; i++) {
			var entry = formEntries[i];
			var name = entry.name;
			var value = entry.value;
			if (name == "itemName" && (value == null || value.length < 3)) {
				showSnackbar("Please Enter a valid item name.");
				error = true;
			}
			if (name == "itemPrice" && (value == null || value < 1)) {
				showSnackbar("Please Enter a valid item price.");
				error = true;
			}
			if (error) {
				$('[name="'+ name + '"]').focus();
				return;
			}
			itemObj[name] = value;
		}
		catalogue.push(itemObj);
		BusinessesColl.doc(bizId).update({catalogue: catalogue}).then(()=>{
			clearForm('manage_add_items_form');
			$('#add_item_form').fadeOut().hide();
		}).catch((err) =>{
			showSnackbar(err.message);
		});
	});
});

$('#view_cart').on('click', function(){
	$('#biz_items').hide();
	$('#cart_items').show();
	$('#cart_items_list').empty();
	cartItems.forEach((item) =>{
		var cartItem = `<li class="cart-item">
	            			<div class="w3-row">
	            				<div class="w3-col" style="width: 60%">${item.itemName}</div>
	            				<div class="w3-col text-center" style="width: 25%">${item.itemPrice}</div>
	            				<div class="w3-col text-right" style="width: 15%">
	            					<i class="ti-trash"></i>
	            				</div>
	            			</div>
	            		</li>`;
	    $('#cart_items_list').append(cartItem);
	});
});

$('#form_registration').on('submit', function(e){
	e.preventDefault();
	var formObj = {owner: currentNickname};
	var error = false;
	var formEntries = JSON.parse(JSON.stringify(jQuery('#form_registration').serializeArray()));
	for (var i = 0; i < formEntries.length; i++) {
		var entry = formEntries[i];
		const value = entry.value;
		const name = entry.name;
		if (name == "name" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid business name.");
			error = true;
		}
		if (name == "description" && (value == null || value.length < 6)) {
			showSnackbar("Please Enter a valid business description.");
			error = true;
		}
		if (name == "addressLine1" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid Street.");
			error = true;
		}
		if (name == "addressCity" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid City.");
			error = true;
		}
		if (name == "email" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid email.");
			error = true;
		}
		if (name == "office_number" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid Office number.");
			error = true;
		}
		if (error) {
			$('[name="'+ name + '"]').focus();
			return;
		}
		formObj[name] = value;
	}
	BusinessesColl.doc().set(formObj).then(() =>{
		clearForm('form_registration');
	}).catch((err) =>{
		showSnackbar(err.message);
	});
});

function addToCart(obj){
	const index = cartItems.findIndex((e)=>e.itemName === obj.itemName);
	if (index == -1) {
		cartItems.push(obj);
	}else{
		cartItems[index] = obj;
	}

}

function onProfileChanged(nickname, avatarPath) {
	currentNickname = nickname
	currentAvatarPath = avatarPath
}

function clearForm(formId){
	$('#' + formId).find("input, textarea").val("");
}

function showSnackbar(message){
	console.log(message);
}

function increaseValue(price) {
  
}

function decreaseValue(price) {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? value = 2 : '';
  value--;
  document.getElementById('number').value = value;
  var subTotal = (+value * +price).toFixed(2);
  $('.sub-price').text("R" + subTotal);
}